function md_format(){
    var converter = new Showdown.converter();
    var b = document.getElementsByClassName("md-text");
    if (!b) return;
    for (i in b){
        if(!b[i].innerHTML) break;
        b[i].innerHTML = converter.makeHtml(b[i].innerHTML);
        b[i].className += " md-formatted";
    }
}

function md_attach_to_tag(tag, classname){
    /* attaches classes to pre and code */
    x=document.getElementsByTagName(tag)
    for (i in x){
        if (x[i].className == "not-md") continue;
        x[i].className=classname;
    }
}

