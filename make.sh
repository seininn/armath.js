#!/bin/bash
ver=1
script="build/armath.ver$ver.js"


die(){
    echo "Fatal error, aborting!"
    exit 1

}


[[ -d "source" ]] || die
[[ -d "build" ]] || mkdir "build" || die

echo "
/*

    $script
    
    THIS IS A MESSY, COMPILED script. reffer to the source distribution
    for a more decent mess of code.
    
    2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>   
    armathjs is public domain software, do with it as you please.
    
    website: http://people.sigh.asia/~sulaiman/software/libraries/armath.js
    
    Sighteam Software (0x1)

*/    

" > $script || die

echo "var css_layout_def=''+" >> $script || die
cat source/layout.css | while read -r x; do echo "'$x\n'+"; done >> $script || die
echo "'';" >> $script || die

cat source/*.js >> $script || die
cp $script ./info


echo "Build sucessfull ('$script')"

exit 0
