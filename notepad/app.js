/*
    api
    update_content(element, content)    :       update dom and array
    edit()                              :       controls the cursor (textarea)
    
*/

// struct data
var content = [];
var revisions = [];

var cursor;                 // cursor DOM
var editing = undefined;    // cursor's cell if active
var page_name = "";         // document's name
var panel;                  // current panel if visible

function init(){
    cursor = document.getElementById("cursor");
    cursor.focus();
    cursor.parentElement.removeChild(cursor);
    dialog_pages_new();
}
