
/*

    build/armath.ver1.js
    
    THIS IS A MESSY, COMPILED script. reffer to the source distribution
    for a more decent mess of code.
    
    2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>   
    armathjs is public domain software, do with it as you please.
    
    website: http://people.sigh.asia/~sulaiman/software/libraries/armath.js
    
    Sighteam Software (0x1)

*/    


var css_layout_def=''+
'/*\n'+
'armathjs: layout.css\n'+
'\n'+
'2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>\n'+
'armathjs is public domain software.\n'+
'\n'+
'XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX\n'+
'\n'+
'Warning: do NOT use single quotes in this file. use double-quotes\n'+
'instead.\n'+
'\n'+
'XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX\n'+
'\n'+
'\n'+
'\n'+
'*/\n'+
'\n'+
'.armath-eq {\n'+
'display: inline-block;\n'+
'font-size:1.8em;\n'+
'font-family:monospace;\n'+
'padding: 10px;\n'+
'margin:0px;\n'+
'\n'+
'}\n'+
'.armath-eq-null{\n'+
'margin:0px;\n'+
'padding:0px;\n'+
'width:1px;\n'+
'height:1em;\n'+
'}\n'+
'\n'+
'\n'+
'.armath-eq-scale-x{\n'+
'transform:  scaleX(2);\n'+
'-webkit-transform: scaleX(2);\n'+
'-o-transform: scaleX(2);\n'+
'-ms-transform: scaleX(2);\n'+
'-moz-transform: scaleX(2);\n'+
'}\n'+
'\n'+
'.armath-eq-operator{\n'+
'margin-left:.2em;\n'+
'margin-right:.2em;\n'+
'}\n'+
'.armath-eq-heavy{\n'+
'font-weight:bold;\n'+
'}\n'+
'.armath-eq-function{\n'+
'font-size:1.1em;\n'+
'/*\n'+
'transform:  skewX(-10deg);\n'+
'-webkit-transform: skewX(-10deg);\n'+
'-o-transform: skewX(-10deg);*/\n'+
'\n'+
'}\n'+
'.armath-eq-larger{\n'+
'margin-right:.4em;\n'+
'margin-left:.4em;\n'+
'margin-bottom:.4em;\n'+
'font-size:1.6em;\n'+
'}\n'+
'.armath-eq-stool{\n'+
'margin-bottom: 0.3em;\n'+
'}\n'+
'.armath-eq-larger > .armath-eq-sub-2{\n'+
'font-size:30%;\n'+
'text-align:center;\n'+
'}\n'+
'\n'+
'.armath-eq-smaller{\n'+
'font-size:.6em;\n'+
'font-weight:100;\n'+
'}\n'+
'.armath-eq-sub{\n'+
'font-size:65%;\n'+
'}\n'+
'.armath-eq-sub-2{\n'+
'font-size:40%;\n'+
'text-align:center;\n'+
'}\n'+
'.armath-eq-inline {\n'+
'display: inline-block;\n'+
'}\n'+
'.armath-eq-common {\n'+
'vertical-align: middle;\n'+
'position:relative;\n'+
'text-align:right;\n'+
'}\n'+
'\n'+
'.armath-eq-vertical-element{\n'+
'text-align: center;\n'+
'width: 100%;\n'+
'display:block;\n'+
'}\n'+
'\n'+
'\n'+
'.armath-eq-horizontal-gap{\n'+
'width:1em;\n'+
'height:1em;\n'+
'}\n'+
'.armath-eq-vertical-gap{\n'+
'width:.2em;\n'+
'height:3em;\n'+
'}\n'+
'\n'+
'.armath-eq-position-bottom{\n'+
'bottom:-1.2em;\n'+
'position:relative;\n'+
'}\n'+
'\n'+
'.armath-eq-position-top{\n'+
'top:-0.9em;\n'+
'position:relative;\n'+
'}\n'+
'\n'+
'.armath-eq-position-center{\n'+
'position:absolute;\n'+
'right:0px;\n'+
'left:0px;\n'+
'padding:0px;;\n'+
'white-space: nowrap;\n'+
'text-align:center;\n'+
'}\n'+
'\n'+
'.armath-eq-fraction{\n'+
'margin:5px;\n'+
'text-align:center;\n'+
'}\n'+
'\n'+
'\n'+
'\n'+
'.armath-eq-margins{\n'+
'margin-left:.1em;\n'+
'margin-right:.1em;\n'+
'}\n'+
'.armath-eq-parentheses{\n'+
'padding-left:.3em;\n'+
'padding-right:.3em;\n'+
'border-left:.1em solid black;\n'+
'border-right:.1em solid black;\n'+
'border-radius: .5em;\n'+
'}\n'+
'\n'+
'.armath-eq-square-brackets{\n'+
'padding:.2em;\n'+
'border-left:2px solid black;\n'+
'border-right:2px solid black;\n'+
'}\n'+
'.armath-eq-square-brackets-tl{\n'+
'position:absolute;\n'+
'left:0px;\n'+
'top:0px;\n'+
'width:.3em;\n'+
'height:3px;\n'+
'background-color: black;\n'+
'}\n'+
'.armath-eq-square-brackets-tr{\n'+
'position:absolute;\n'+
'right:0px;\n'+
'top:0px;\n'+
'width:.3em;\n'+
'height:3px;\n'+
'background-color: black;\n'+
'}\n'+
'.armath-eq-square-brackets-bl{\n'+
'position:absolute;\n'+
'left:0px;\n'+
'bottom:0px;\n'+
'width:.3em;\n'+
'height:3px;\n'+
'background-color: black;\n'+
'}\n'+
'.armath-eq-square-brackets-br{\n'+
'position:absolute;\n'+
'right:0px;\n'+
'bottom:0px;\n'+
'width:.3em;\n'+
'height:3px;\n'+
'background-color: black;\n'+
'}\n'+
'\n'+
'.armath-eq-absolute{\n'+
'padding:.1em;\n'+
'border-left:2px solid black;\n'+
'border-right:2px solid black;\n'+
'}\n'+
'\n'+
'.armath-eq-root-container{\n'+
'padding-right:.2em;\n'+
'}\n'+
'.armath-eq-root-degree{\n'+
'position:absolute;\n'+
'top:0px;\n'+
'right:-.5em;\n'+
'}\n'+
'.armath-eq-root-falling{\n'+
'border-right: 2px solid black;\n'+
'height: 50%;\n'+
'bottom: 0px;\n'+
'position: absolute;\n'+
'right: -.2em;\n'+
'transform: skewX(-10deg);\n'+
'-webkit-transform: skewX(-10deg);\n'+
'-o-transform: skewX(-10deg);\n'+
'-ms-transform: skewX(-10deg);\n'+
'-moz-transform: skewX(-10deg);\n'+
'}\n'+
'.armath-eq-root-dec{\n'+
'position:absolute;\n'+
'top:0px;\n'+
'bottom:0px;\n'+
'left:0px;\n'+
'right:0px;\n'+
'border-top: 2px solid black;\n'+
'border-right: 2px solid black;\n'+
'transform:  skewX(5deg);\n'+
'-webkit-transform: skewX(5deg);\n'+
'-o-transform: skewX(5deg);\n'+
'-ms-transform: skewX(5deg);\n'+
'-moz-transform: skewX(5deg);\n'+
'}\n'+
'.armath-eq-root-body{\n'+
'padding:.25em;\n'+
'}\n'+
'.armath-eq-flip{\n'+
'transform:  scaleX(-1);\n'+
'-webkit-transform: scaleX(-1);\n'+
'-o-transform: scaleX(-1);\n'+
'-ms-transform: scaleX(-1);\n'+
'-moz-transform: scaleX(-1);\n'+
'}\n'+
'\n'+
'.armath-eq-hr{\n'+
'border: none;\n'+
'height:2px;\n'+
'background-color: black;\n'+
'margin-top: .1em;\n'+
'margin-bottom: .2em;\n'+
'right:-5px;\n'+
'left:-5px;\n'+
'}\n'+
'\n'+
'\n'+
'\n'+
'.armath-eq-curly-container{\n'+
'padding-right:2em;\n'+
'padding-top:1em;\n'+
'padding-bottom:1em;\n'+
'}\n'+
'\n'+
'.armath-eq-curly-top-right{\n'+
'position:absolute;\n'+
'width:1em;\n'+
'height:40%;\n'+
'top:10%;\n'+
'right:0px;\n'+
'border-left: 2px solid black;\n'+
'border-radius: 0px 0px 0px .5em;\n'+
'}\n'+
'\n'+
'.armath-eq-curly-bottom-right{\n'+
'position:absolute;\n'+
'width:1em;\n'+
'height:40%;\n'+
'bottom:10%;\n'+
'right:0px;\n'+
'border-left: 2px solid black;\n'+
'border-radius: .5em 0px 0px 0px;\n'+
'}\n'+
'\n'+
'\n'+
'.armath-eq-curly-top-left{\n'+
'position:absolute;\n'+
'top:5px;\n'+
'bottom:90%;\n'+
'right:1em;\n'+
'width:1em;\n'+
'border-right: 2px solid black;\n'+
'border-radius: 0px 1em 0px 0px;\n'+
'}\n'+
'\n'+
'.armath-eq-curly-bottom-left{\n'+
'position:absolute;\n'+
'bottom:5px;\n'+
'top:90%;\n'+
'right:1em;\n'+
'width:1em;\n'+
'border-right: 2px solid black;\n'+
'border-radius: 0px 0px 1em 0px;\n'+
'}\n'+
'\n'+
'.armath-eq-bar{\n'+
'border-top: 2px solid black;\n'+
'}\n'+
'\n'+
'.armath-eq-matrix{\n'+
'border-collapse:collapse;\n'+
'border-spacing:0;\n'+
'}\n'+
'\n'+
'.armath-eq-matrix-element{\n'+
'padding-right:.3em;\n'+
'padding-left:.3em;\n'+
'\n'+
'}\n'+
'\n'+
'.armath-eq-vertical-subelement-gap{\n'+
'padding-bottom:.3em;\n'+
'}\n'+
'';
/*
    armathjs: core.js
    2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>    
    armathjs is public domain software.
    
*/

if (typeof armath_translate_numrals === "undefined") var armath_translate_numrals = true;

/* internal */
var armath_is_init = false;

//arabic_indexof

function arabic_is_abjd(c){
    // abjdbet: 0x621 -> 0x63A; 0x640 -> 0x64A
    // harakat: 0x64B -> 0x655
    c=c.charCodeAt();
    if ( (c >= 0x621 && c <= 0x63A) || 
         (c >= 0x640 && c <= 0x64A) || 
         (c >= 0x64B && c <= 0x655)) return true;
    else return false;
}

function arabic_is_numral(c){
    // arab-indic numrals: 0x660 -> 0x669
    // arabic numrals: 0x30 -> 0x39
    c=c.charCodeAt();
    if ( (c >= 0x660 && c <= 0x669) ||
         (c >= 0x30 && c <= 0x39) ||
         (c == 44) || (c == 46)
         ) return true;
    else return false;
}

function arabic_is_abjdnumric(c){
    return arabic_is_abjd(c) || arabic_is_numral(c);
}



function arabic_follow_abjd(str){
    var len=str.length;
    for (i=0; i<len; ++i){
        if (! arabic_is_abjd(str[i])) return i;
    }
    return len;
}

function arabic_follow_numral(str){
    var len=str.length;
    for (i=0; i<len; ++i){
        if (! arabic_is_numral(str[i])) return i;
    }
    return len;
}

/*
    function arabic_is_unjoined(bc)
    
    returns true if 
        A: a single character string is abjdnumric
        B: the second character in the string is abjdnumric and is not proceeded
           by or followed by another abjdnuric.
*/
function arabic_is_unjoined(bc){ 
    return  (
                (bc.length == 1) && arabic_is_abjdnumric(bc)
            ) ||
            (
                !arabic_is_abjdnumric(bc[0]) && 
                (
                    (bc.length > 1) && arabic_is_abjdnumric(bc[1])
                ) && 
                (
                    (bc.length < 3) || !arabic_is_abjdnumric(bc[2])
                )
            )
}

/* this function has two seprate logical algorithims fused together
if you can wrap your head around it, remove all the diff/samepara 
from it*/

function armath_get_parentheses(str, c){
    var pcount=0;
    var i=0;
    
    if(!c) c=["(", ")"];

    if (str[0] != c[0]) return -1;
    
    // for same paranthese
    if(c[0] == c[1]) {
        for (i=1; i<str.length && str[i]!=c[0]; ++i);
        if (i==str.length) return -1
        else return i+1;
    }
    
    // for different paranthese
    for (; i<str.length; ++i){
        if (str[i] == c[0]) ++pcount;
        else if (str[i] == c[1]) --pcount;
        if (!pcount) break;
    }
    if (pcount) return -1;
    else return i+1;
}

function armath_new_node(classes, textcontent){
    var e = document.createElement('div');
    if (classes) e.setAttribute("class", classes);
    if (textcontent) e.textContent=textcontent;
    return e;
}


function armath_nodify_arg(classes, str){
    var e = armath_new_node(classes);
    var pos;
    
    if (str[0]=="("){
        var pl=armath_get_parentheses(str);
        if (pl < 0) {
            console.warn(   "no closing parantheses found"); 
            return;
        }
        pos = armath_compile(e, str.substring(1, pl-1));
        return [e, pl];    
    } 
    else if (pos=arabic_follow_abjd(str)){
        e.textContent=str.substring(0, pos);
        return [e, pos];
    }
    else if (pos=arabic_follow_numral(str)){
        e.textContent=str.substring(0, pos);
        return [e, pos];
    }
    else return;
}



function armath_fill_tree(parent, tree, str, mod, o){
    var pos = 0, te;
    if (!o) o=[undefined]; // o used as a pointer to allow recursed func modif
    

    // packing modifiers: for inclusion in last div or for taking last div as 
    // first input
        
    if (mod && mod.indexOf("reverse-pack")>-1){
        o = [parent.removeChild(parent.lastChild)]; 
        if ((mod.indexOf("remove-parentheses")>-1) && 
                (o[0].getAttribute("class").indexOf("armath-eq-parentheses")>-1)){
            o[0].setAttribute("class", "armath-eq-common armath-eq-inline");
        }
        
    }
    else if (mod && mod.indexOf("reverse-embed")>-1){
        parent = parent.lastChild; 
    }


    if (typeof tree === "string"){
        parent.appendChild( document.createTextNode( tree));
        return 0;
    } 
    
    else if (typeof tree === "object"){
        if (typeof tree.length === "undefined") {
            console.error("malformed lookup tree: please submit a bug report.");
            return;
        }
        for (var i=0;i<tree.length; ++i){
            // if payload undefined thus requires data from str
            if(typeof tree[i][1] === "undefined"){ 
                if (o[0]){
                    parent.appendChild(o[0]);
                    o[0]=false;
                }
                else{
                    te = armath_nodify_arg(tree[i][0], str.substring(pos));
                    if (!te) return;
                    pos += te[1];
                    parent.appendChild(te[0]);
                }            
            }
            
            // if object in which case it must be an array or a string
            else if (typeof tree[i][1] === "object") { 
                te=armath_new_node(tree[i][0]);
                parent.appendChild(te);
                pos += armath_fill_tree(   te, 
                                    tree[i][1], 
                                    str.substring(pos), undefined, o);
            } 
            else if (typeof tree[i][1] === "string") { 
                te=armath_new_node(tree[i][0], tree[i][1]);
                parent.appendChild(te);
            } 
            else {
                console.error(
                    "malformed lookup tree: please submit a bug report.");
                return;
            }
        }
    } 
    else {
        console.error("malformed lookup tree: please submit a bug report.");
        return;
    }
    
    return pos;
}


function armath_compile(parent, str){
    var pos = 0;
    var tnode, tstr, tlen, tpos, te;
    /******
        static variables 
    ******/
    
    armath_compile.comm="armath-eq-common armath-eq-inline armath-eq-margins";
    
    /* --- logic start ---*/
    
    // rejecting invalid armath code
    if(typeof str !== "string" || str == "") return 0;
    
    // translate numrals if 'armath_translate_numrals' has been set
    if(armath_translate_numrals) for(var i = 0; i<10;++i) str = 
                                str.replace(new RegExp(i,"g"), num_lookup[i]);

    for (; pos<str.length; ++pos){
        
        //console.log(parent.lastChild, pos, str.substring(pos));
        
        if (false);
        
        /* (0)  if none of the above filters have been triggered, 
                handle as an odd symbol */
                
        else if (false){
        
        
        }
        
        
        /* (0) verticle vectors (inf args) */
                
        else if (str[pos] == "¦"){
            pos+=1;
            tnode = armath_new_node(
                "armath-eq-common armath-eq-common armath-eq-inline armath-eq-margins");
            
            while(pos<str.length){
                te=armath_nodify_arg("armath-eq-common armath-eq-vertical-element armath-eq-vertical-subelement-gap", str.substring(pos));
                if (!te) break;
                tnode.appendChild(te[0]);
                pos+=te[1];
            }
            parent.appendChild(tnode);
            pos-=1;
        }
        
        else if (str[pos] == "#") {
            var tr, td;
            tnode = document.createElement('table');
            tnode.setAttribute("class", "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-matrix");;
            
            while (pos<str.length && str[pos] == "#") {
                pos+=1;
                tr=document.createElement('tr');
                
                while(pos<str.length){
                    td=document.createElement('td');
                    td.setAttribute("class", "armath-eq-matrix-element armath-eq-vertical-subelement-gap");
                    te=armath_nodify_arg("armath-eq-common armath-eq-vertical-element", str.substring(pos));
                    if (!te) break;
                    td.appendChild(te[0]);
                    tr.appendChild(td);
                    pos+=te[1];
                }
                tnode.appendChild(tr);
                while (str[pos]==" " || str[pos]=="\t" || str[pos]=="\n") pos+=1;
            }
            
            parent.appendChild(tnode);   
            
            pos-=1;
        }
        
        /* (0)  if the current character matchs the overridable 
                directive symbol defined in the global 
                configuration section above */
                
        else if (str[pos] == "$"){
            tlen = arabic_follow_abjd(str.substring(pos+1));
            
            // return the same char if $ is a loner
            if (!tlen) parent.appendChild(
                armath_new_node(armath_compile.comm, "$"));
            
            var drctv = str.substring(pos+1, pos+tlen+1);
            if (!(drctv in lookup)) {
                console.warn("'" + drctv + "' is not a valid armath directive.");
                continue;
            }
            pos+=(tlen+1);
            // checkpoint A: checked
            if (drctv in lookup_mod) tstr=lookup_mod[drctv];
            else tstr = undefined;
            
            pos+= armath_fill_tree(parent, lookup[drctv], str.substring(pos), tstr);
            pos-=1; // to account for the auto invrement in this for loop
        }



        /* (1)  non abjdic directives MUST HAVE A lookup_mod ENTRY
                blank is okay */
        else if (str[pos] in lookup_mod){
            pos+= armath_fill_tree(
                        parent, 
                        lookup[str[pos]], 
                        str.substring(pos+1), 
                        lookup_mod[str[pos]]
                    ); 
        }
        
        
        /* (2)  surround the following paretheses block with 
                css parenthese */
        
        else if (str[pos]=="("){
            pos+= armath_fill_tree(parent, lookup["parentheses"], str.substring(pos));
            pos-=1;// for autoinvrement
          
        }
        else if (str[pos]=="|"){
            tlen=armath_get_parentheses(str.substring(pos), ["|", "|"]);
            if (tlen == -1){
                console.warn("no closing mark for an open '|'");
                continue;
            }
            armath_fill_tree(parent, lookup["absolute"], 
                "("+str.substring(pos+1, pos+tlen-1)+")");
            pos+=tlen-1;// for autoinvrement
          
        }
        else if (str[pos]=="["){
            tlen=armath_get_parentheses(str.substring(pos), ["[", "]"]);
            if (tlen == -1){
                console.warn("no closing mark for an open '['");
                continue;
            }
            armath_fill_tree(parent, lookup["square-brackets"], 
                "("+str.substring(pos+1, pos+tlen-1)+")");
            pos+=tlen-1;// for autoinvrement
          
        }
        
        /* (3)  consecutive numbers or letters  */
        else if (tpos=arabic_follow_abjd(str.substring(pos))){
            tstr=str.substring(pos, pos+tpos);
            parent.appendChild( armath_new_node( armath_compile.comm, tstr));
            pos+=tpos-1;
        }
        else if (tpos=arabic_follow_numral(str.substring(pos))){
            tstr=str.substring(pos, pos+tpos);
            parent.appendChild( armath_new_node( armath_compile.comm, tstr));
            pos+=tpos-1;
        }
        
        /* (-)  if none of the above filters have been triggered, 
                handle as an odd symbol */
        else {
            if ("\t\n ".indexOf(str[pos])>=0) continue;
            else if (str[pos] in lookup){
                // note: although pos has the potential of being updated here,
                // it realy, realy shouldn't.
                armath_fill_tree(parent, lookup[str[pos]], str.substring(pos));
            }
            else if (str[pos] == "@"){
                tstr = str[pos+1];
                parent.appendChild( armath_new_node( armath_compile.comm, tstr));
                pos+=1;
            }
            else{
                tstr = str[pos];
                parent.appendChild( armath_new_node( armath_compile.comm, tstr));
            }
        }
        
    }
    return pos;
}


function armath_init(){
    if (armath_is_init) return;
    var style=document.createElement("style");
    style.setAttribute("type", "text/css");
    //style.setAttribute("media", "screen, print");
    style.textContent=css_layout_def;
    document.getElementsByTagName("head")[0].appendChild(style);
    armath_is_init=true;
}

function armath_render_string(s)
{   
    armath_init();
    var n=armath_new_node("armath-eq");
    armath_compile(n, s);
    return n.outerHTML;
}
function armath_render(){
    armath_init();
    var lista=document.getElementsByClassName("armath-eq");
    var temp;
    for (i in lista) {
        temp=lista[i].innerHTML;
        lista[i].innerHTML="";
        armath_compile(lista[i], temp);
    }
                
}





/*
    armathjs: lookup.js: contains the main translation table
    
    2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>    
    armathjs is public domain software.
    
    
    
    FORMAT
    
        lookup is used to translate armath directives to html. it has the 
        following structure*:
        
            lookup[d] = [ A ]
        
        where [A] is an array of As, and A is defined as:
        
            A = [ B, C ]
        
        each A is translated to a separate div. B is a string of classes 
        and must be provided ("" are okay). C can be:
        
            1.  `undefined` to sigify that armath should read from the 
                formating string.
            
            2.  a string which is inserted in the div
            
            3.  [ A ] for sub elements
        
        *lookup[d] can also be a string, and, if so, is simpally appended 
        to the parent element.


    LOOKUP_MOD
        
        lookup_mod is a companion array that modifies the behaviour of 
        armath. it holds a string of space seperated options which can
        be:
        
            1.  `reverse-pack` which indicates that the `undefined` 
                value should not be read from the formatting string
                but be the last div in the parent.
            
            2.  `reverse-embed` which indecates that the directive 
                should be rendered inside the last div in the parent.
               
            3. `remove-parentheses` removes the parantheses of the 
                last div. (only works with `reverse-pack`)
                
                
    CHARACTER_LOOKUP
    
        An key:value list characters that should be repaced should they 
        occure sepratly inside a div.
        
        
*/

var num_lookup=[ '٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩' ];

/**/
var lookup = new Array();
var lookup_mod = new Array(); // reverse pickup


lookup["إلى"]=
                '←';    
lookup["من"]=
                '→'; 
lookup["لانهاية"]=
                '∞'; 



// A: ثوابت
lookup["باي"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "π"]];
lookup["ثيتا"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "θ"]];
lookup["فاي"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "∅"]];
lookup["لامدا"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "λ"]];
lookup["دل"]=[["armath-eq-inline armath-eq-common armath-eq-margins armath-eq-flip", "∂"]];
lookup["دلتا"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "Δ"]];


// A: المجموع والمضروب
lookup["مجموع"]=[["armath-eq-inline armath-eq-common armath-eq-stool", "ﳎ"]];
lookup["مضروب"]=[["armath-eq-inline armath-eq-common", "∏"]];

// calculus
lookup["تكامل"]=[
    [
        "armath-eq-inline armath-eq-common",
        [
            ["armath-eq-flip", "∫"]
        ]
    ]
];


// A: معادلات
lookup[">"]=lookup["أكبر"]=[["armath-eq-inline armath-eq-common armath-eq-operator", ">"]];
lookup["<"]=lookup["أصغر"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "<"]];
lookup["="]=lookup["يساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "="]];
lookup["+"]=lookup["موجب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "+"]];
lookup["-"]=lookup["سالب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "−"]];
lookup["*"]=lookup["ضرب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "×"]];
lookup["مولب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "±"]];
lookup["ساجب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "∓"]];
lookup["لايساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≠"]];
lookup["أكبريساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≥"]];
lookup["أصغريساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≤"]];
lookup["تقريب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≈"]];
lookup["تعريف"]=[
    [
        "armath-eq-inline armath-eq-common armath-eq-operator", 
            [
                ["", "="],
                ["armath-eq-common armath-eq-inline armath-eq-position-top armath-eq-position-center armath-eq-sub-2", "تعر"]
            ]
    ]
]
// A: منطق
lookup["جمع"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "⊕"]];
lookup["ضرب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "⊗"]];


// A: منطق
lookup["لكل"]='∀';
lookup["إذا"]='∴';
lookup["بماأن"]='∵';
lookup["مكافئ"]='⇔';
lookup["ينتج"]='⇒';
lookup["يؤدي"]='⇒';




// A: رسم
lookup["فراغ"]=[["armath-eq-common armath-eq-inline armath-eq-margins", ""]];
lookup["فأفقي"]=[["armath-eq-common armath-eq-inline armath-eq-horizontal-gap", ""]];
lookup["فرأسي"]=[["armath-eq-common armath-eq-inline armath-eq-vertical-gap", ""]];

lookup["ضخم"]=[["armath-eq-common armath-eq-inline armath-eq-heavy", undefined]];
lookup["دالة"]=[["armath-eq-common armath-eq-inline armath-eq-function", undefined]];
lookup["كبير"]=[["armath-eq-common armath-eq-inline armath-eq-larger", undefined]];
lookup["صغير"]=[["armath-eq-common armath-eq-inline armath-eq-smaller", undefined]];





lookup["^"]=[
    [
        "armath-eq-common armath-eq-inline", 
            [
                ["", undefined],
                ["armath-eq-common armath-eq-inline armath-eq-position-top armath-eq-sub armath-eq-margins", undefined]
            ]
    ]
];
lookup_mod["^"]="reverse-pack";

lookup["فوق"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-position-top armath-eq-position-center armath-eq-sub-2", undefined
    ]
];
lookup_mod["فوق"]="reverse-embed";



lookup["_"]=[
    [
        "armath-eq-common armath-eq-inline", 
            [
                ["", undefined],
                ["armath-eq-common armath-eq-inline armath-eq-position-bottom armath-eq-sub armath-eq-margins", undefined]
            ]
    ]
];
lookup_mod["_"]="reverse-pack";

lookup["تحت"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-position-bottom armath-eq-position-center right armath-eq-sub-2", undefined
    ]
];
lookup_mod["تحت"]="reverse-embed";


         
lookup["parentheses"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-parentheses",
        undefined
    ]
];

lookup["absolute"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-absolute",
        undefined
    ]
];

lookup["square-brackets"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-square-brackets",
            [
                ["armath-eq-common armath-eq-inline", undefined],
                ["armath-eq-square-brackets-tl", ""],
                ["armath-eq-square-brackets-tr", ""],
                ["armath-eq-square-brackets-bl", ""],
                ["armath-eq-square-brackets-br", ""]
            ]
    ]
];

lookup["سقف"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-square-brackets",
            [
                ["armath-eq-common armath-eq-inline", undefined],
                ["armath-eq-square-brackets-tl", ""],
                ["armath-eq-square-brackets-tr", ""]
            ]
    ]
];

lookup["قاع"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-square-brackets",
            [
                ["armath-eq-common armath-eq-inline", undefined],
                ["armath-eq-square-brackets-bl", ""],
                ["armath-eq-square-brackets-br", ""]
            ]
    ]
];




lookup["ند"]= [
    [
        "armath-eq-common armath-eq-inline armath-eq-fraction armath-eq-bar", undefined
    ]
];        

lookup["\\"]=
lookup["كسر"]= [
    [
        "armath-eq-common armath-eq-inline armath-eq-fraction armath-eq-margins", 
        [
            ["armath-eq-common armath-eq-inline", undefined],
            ["armath-eq-hr", ""],
            ["armath-eq-common armath-eq-inline armath-eq-fraction-denominator", undefined]
        ]
    ]
];        
lookup_mod["\\"]="reverse-pack remove-parentheses";

lookup["/"]=
lookup["جذر"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-root-container armath-eq-margins",
            [
                [
                    "armath-eq-sub armath-eq-root-degree", undefined
                ],
                [
                    "armath-eq-common armath-eq-inline armath-eq-root-body",
                    [
                        ["armath-eq-common armath-eq-inline armath-eq-root-dec", ""],
                        ["", undefined],
                        ["armath-eq-common armath-eq-inline armath-eq-root-falling", ""]
                    ]
                ]
            ]
    ]
];
lookup_mod["/"]="";



lookup["{"]=
lookup["أفرع"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-curly-container",
            [
                [ "armath-eq-curly-top-left", ""],
                [ "armath-eq-curly-top-right", ""],
                [ "armath-eq-curly-bottom-left", ""],
                [ "armath-eq-curly-bottom-right", ""],
                [ "armath-eq-common armath-eq-inline", undefined ]
            ]
    ]
];
lookup_mod["{"]="";


/*  Character translation lookup  [TODO]
    ============================
    
    This is a direct subsitution lookup dictionary where the key 
    should be replaced with the value.

*/
var character_lookup= new Array();

character_lookup["ن"]="Ͽ";
