function edit(d, action){
    if (action == "done" || (editing && action == "edit")) {
        update_content(editing, cursor.value);
        cursor.parentElement.insertBefore(editing, cursor);
        cursor.parentElement.removeChild(cursor);
        editing.focus();
        editing = undefined;
        if (action == "done") return;
    }
    
    /* attaching cursor */
    editing = d;
    d.parentElement.insertBefore(cursor,  d);
    cursor.tabIndex = editing.tabIndex;
    d.parentElement.removeChild(d);
    cursor.focus();
    cursor.value=content[d.getAttribute("name")];
}

function cursor_key(e){
    if (e.which == 27 || (e.which == 13 && event.shiftKey)) {
        edit(undefined, "done");
        return false;
    }
    return true;
}

function cursor_paste(event){
    event.stopImmediatePropagation();
    var i, d, f, items, blob, reader;    
    items = event.clipboardData.items;
    d = editing;
    
    if (items[0].type.substring(0, 5) != "image") return false;
    
    blob = items[0].getAsFile();
    if (!blob) {console.warn("not a blob"); return false;}
    reader = new FileReader();
    edit(undefined, "done");
    reader.onload = function(event){
        if (cursor.value == "" ) {
            update_content(d, reader.result); 
        }
        else {
            f = update_content(undefined, reader.result);
            d.parentElement.insertBefore(d, f);
        }
        reindex();
    }; 
    reader.readAsDataURL(blob);
    event.preventDefault();
    return false;
}
