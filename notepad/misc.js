function bottom_expand(){
    update_content(undefined, "").focus();
}

function object_key(e, d){
    if (event.shiftKey) {
        if (e.which == 13 ) {
            edit(d, "edit");
            return false;
        }
        if (e.which == 8 || e.which == 46) {
            remove_content(d);
            return false;
        }
        if (e.which == 83 ) {
            remove_content(d);
            return false;
        }
    }
    if (e.which == 27){
        document.getElementById("content").focus();
    }
    return true;
}

