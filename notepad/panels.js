function dialog_close(){
    if (panel) document.getElementById("notepad").removeChild(panel);
    panel = undefined;
}
function dialog_pages_new(){
    content_loader('');
    page_name='';
    edit(update_content(undefined, ""), "edit");    
    dialog_close();
}
function dialog_pages_title(t){
    page_name = t.value; 
    document.getElementById('dialog-pages-save').setAttribute('download', (page_name?page_name:'صفحة')+'.صدب');
    document.title=page_name;
}

function dialog_pages_load(evt){
    if (editing) edit(undefined, "done");
    if(!evt.target.value) return;
    var file = evt.target.files[0]; 
    page_name=file.name.split(".")[0];
    var r = new FileReader();
    r.onload = function(event){
        content_loader(r.result);
    }; 
    r.readAsText(file);
    evt.target.value="";
    dialog_close();
}

function dialog_pages(){ // rename pages to dialog-files  and nav to dialog
    window.URL = window.URL || window.webkitURL ;
    window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder;

    var url, data = "";
    var bb = new window.BlobBuilder()
    var e;
    

    if (!dialog_pages.panel){ // with other initialization code!
        dialog_pages.panel = document.getElementById("dialog-files");
    }
    
    if(panel) { 
        dialog_close();
        return;
    }
    
    panel = dialog_pages.panel;
    
    //window.URL.revokeObjectURL(url) and make url static

    for (var i = 0; i<content.length; ++i) if (content[i]) data += content[i] + String.fromCharCode(27);
    bb.append(data)
    data = bb.getBlob("text\/plain");
    url = window.URL.createObjectURL(data);
    
    document.getElementById("notepad").appendChild(panel);
    
    
    
    panel.style.display="block";
    
    if(page_name != "") document.getElementById("dialog-pages-pagename").setAttribute("value", page_name);
    
    e = document.getElementById("dialog-pages-save");
    e.setAttribute("href", url);
    e.setAttribute('download', (page_name?page_name:'صفحة')+'.صدب');
    e.onclick="dialog_close();";


}
