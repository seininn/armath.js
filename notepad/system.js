function remove_all_content_objects(){
    var c = document.getElementsByClassName("content-object");
    while (c.length) c[0].parentElement.removeChild(c[0]);
}

function save_revision(){
    if (revisions.length >= 5) {
        revisions.shift();    
        revisions.push(content.slice(0));
    }
    else revisions.push(content.slice(0));

    console.log(revisions);
}

function undo(){
    if (revisions.length == 0)  return undefined;
    content = [];
    var data = revisions.pop();
    remove_all_content_objects();
    for (var i = 0; i<data.length; ++i) if(data[i] != undefined) update_content(undefined, data[i], true);
}

function content_loader(data){
    var data;
    
    content = [];
    revisions = [];
    
    remove_all_content_objects();

    data=data.split(String.fromCharCode(27))
    for (var i = 0; i < data.length; ++i) if (data[i]) update_content(undefined, data[i], true);
    
}

function update_content(e, text, ignore_rev){
    if (!ignore_rev) save_revision();
    if (!e) {
        var id = content.length;
        e = document.createElement('div');
        e.setAttribute("class", "content-object");
        e.setAttribute("name", id);
        e.setAttribute("onkeydown", "return object_key(event, this)");
        e.setAttribute("onpaste", "return object_paste(event, this)");
        document.getElementById("content").appendChild(e);   
        content.push(text);
    }
    else {
        content[e.getAttribute("name")] = text;
    }

    html_gen(e);
    reindex();
    return e;
}

function remove_content(d){
    save_revision();
    content[d.getAttribute("name")]=undefined;
    var c = document.getElementsByClassName("content-object");
    
    if (d.tabIndex-1 > 0) {
        for (var i = 0; i < c.length; ++i) {
            if (c[i].tabIndex == d.tabIndex-1 ) {
                c[i].focus();
            }
        }
    }
    
    d.parentElement.removeChild(d);
    reindex();
}

function html_gen(e){
    var converter = new Showdown.converter();
    var text = content[e.getAttribute("name")];
    
    if (text.substr(0, 8) == "$رياضيات") {
        e.innerHTML = armath_render_string(text.substr(8));
    }
    else if (text.substr(0, 5) == "data:") { //expand to more formats via mime types
        e.innerHTML = "<div style='width:100%;text-align:center;'><img style='max-width:70%' src='"+text+"' alt='صورة' ></div>";
    }
    else {
        e.innerHTML = converter.makeHtml(text);
    }
}

function reindex(){
    var i, j=2; x = document.getElementById("cursor-activator");
    
    x.parentElement.appendChild(x); // assure it's last
    
    x = document.getElementById("content").childNodes;
    for (i = 0; i < x.length; ++i) { 
        if (x[i].nodeType == 1) x[i].tabIndex = j++;
        
    }
}
