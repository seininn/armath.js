/*
    armathjs: core.js
    2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>    
    armathjs is public domain software.
    
*/

if (typeof armath_translate_numrals === "undefined") var armath_translate_numrals = true;

/* internal */
var armath_is_init = false;

//arabic_indexof

function arabic_is_abjd(c){
    // abjdbet: 0x621 -> 0x63A; 0x640 -> 0x64A
    // harakat: 0x64B -> 0x655
    c=c.charCodeAt();
    if ( (c >= 0x621 && c <= 0x63A) || 
         (c >= 0x640 && c <= 0x64A) || 
         (c >= 0x64B && c <= 0x655)) return true;
    else return false;
}

function arabic_is_numral(c){
    // arab-indic numrals: 0x660 -> 0x669
    // arabic numrals: 0x30 -> 0x39
    c=c.charCodeAt();
    if ( (c >= 0x660 && c <= 0x669) ||
         (c >= 0x30 && c <= 0x39) ||
         (c == 44) || (c == 46)
         ) return true;
    else return false;
}

function arabic_is_abjdnumric(c){
    return arabic_is_abjd(c) || arabic_is_numral(c);
}



function arabic_follow_abjd(str){
    var len=str.length;
    for (i=0; i<len; ++i){
        if (! arabic_is_abjd(str[i])) return i;
    }
    return len;
}

function arabic_follow_numral(str){
    var len=str.length;
    for (i=0; i<len; ++i){
        if (! arabic_is_numral(str[i])) return i;
    }
    return len;
}

/*
    function arabic_is_unjoined(bc)
    
    returns true if 
        A: a single character string is abjdnumric
        B: the second character in the string is abjdnumric and is not proceeded
           by or followed by another abjdnuric.
*/
function arabic_is_unjoined(bc){ 
    return  (
                (bc.length == 1) && arabic_is_abjdnumric(bc)
            ) ||
            (
                !arabic_is_abjdnumric(bc[0]) && 
                (
                    (bc.length > 1) && arabic_is_abjdnumric(bc[1])
                ) && 
                (
                    (bc.length < 3) || !arabic_is_abjdnumric(bc[2])
                )
            )
}

/* this function has two seprate logical algorithims fused together
if you can wrap your head around it, remove all the diff/samepara 
from it*/

function armath_get_parentheses(str, c){
    var pcount=0;
    var i=0;
    
    if(!c) c=["(", ")"];

    if (str[0] != c[0]) return -1;
    
    // for same paranthese
    if(c[0] == c[1]) {
        for (i=1; i<str.length && str[i]!=c[0]; ++i);
        if (i==str.length) return -1
        else return i+1;
    }
    
    // for different paranthese
    for (; i<str.length; ++i){
        if (str[i] == c[0]) ++pcount;
        else if (str[i] == c[1]) --pcount;
        if (!pcount) break;
    }
    if (pcount) return -1;
    else return i+1;
}

function armath_new_node(classes, textcontent){
    var e = document.createElement('div');
    if (classes) e.setAttribute("class", classes);
    if (textcontent) e.textContent=textcontent;
    return e;
}


function armath_nodify_arg(classes, str){
    var e = armath_new_node(classes);
    var pos;
    
    if (str[0]=="("){
        var pl=armath_get_parentheses(str);
        if (pl < 0) {
            console.warn(   "no closing parantheses found"); 
            return;
        }
        pos = armath_compile(e, str.substring(1, pl-1));
        return [e, pl];    
    } 
    else if (pos=arabic_follow_abjd(str)){
        e.textContent=str.substring(0, pos);
        return [e, pos];
    }
    else if (pos=arabic_follow_numral(str)){
        e.textContent=str.substring(0, pos);
        return [e, pos];
    }
    else return;
}



function armath_fill_tree(parent, tree, str, mod, o){
    var pos = 0, te;
    if (!o) o=[undefined]; // o used as a pointer to allow recursed func modif
    

    // packing modifiers: for inclusion in last div or for taking last div as 
    // first input
        
    if (mod && mod.indexOf("reverse-pack")>-1){
        o = [parent.removeChild(parent.lastChild)]; 
        if ((mod.indexOf("remove-parentheses")>-1) && 
                (o[0].getAttribute("class").indexOf("armath-eq-parentheses")>-1)){
            o[0].setAttribute("class", "armath-eq-common armath-eq-inline");
        }
        
    }
    else if (mod && mod.indexOf("reverse-embed")>-1){
        parent = parent.lastChild; 
    }


    if (typeof tree === "string"){
        parent.appendChild( document.createTextNode( tree));
        return 0;
    } 
    
    else if (typeof tree === "object"){
        if (typeof tree.length === "undefined") {
            console.error("malformed lookup tree: please submit a bug report.");
            return;
        }
        for (var i=0;i<tree.length; ++i){
            // if payload undefined thus requires data from str
            if(typeof tree[i][1] === "undefined"){ 
                if (o[0]){
                    parent.appendChild(o[0]);
                    o[0]=false;
                }
                else{
                    te = armath_nodify_arg(tree[i][0], str.substring(pos));
                    if (!te) return;
                    pos += te[1];
                    parent.appendChild(te[0]);
                }            
            }
            
            // if object in which case it must be an array or a string
            else if (typeof tree[i][1] === "object") { 
                te=armath_new_node(tree[i][0]);
                parent.appendChild(te);
                pos += armath_fill_tree(   te, 
                                    tree[i][1], 
                                    str.substring(pos), undefined, o);
            } 
            else if (typeof tree[i][1] === "string") { 
                te=armath_new_node(tree[i][0], tree[i][1]);
                parent.appendChild(te);
            } 
            else {
                console.error(
                    "malformed lookup tree: please submit a bug report.");
                return;
            }
        }
    } 
    else {
        console.error("malformed lookup tree: please submit a bug report.");
        return;
    }
    
    return pos;
}


function armath_compile(parent, str){
    var pos = 0;
    var tnode, tstr, tlen, tpos, te;
    /******
        static variables 
    ******/
    
    armath_compile.comm="armath-eq-common armath-eq-inline armath-eq-margins";
    
    /* --- logic start ---*/
    
    // rejecting invalid armath code
    if(typeof str !== "string" || str == "") return 0;
    
    // translate numrals if 'armath_translate_numrals' has been set
    if(armath_translate_numrals) for(var i = 0; i<10;++i) str = 
                                str.replace(new RegExp(i,"g"), num_lookup[i]);

    for (; pos<str.length; ++pos){
        
        //console.log(parent.lastChild, pos, str.substring(pos));
        
        if (false);
        
        /* (0)  if none of the above filters have been triggered, 
                handle as an odd symbol */
                
        else if (false){
        
        
        }
        
        
        /* (0) verticle vectors (inf args) */
                
        else if (str[pos] == "¦"){
            pos+=1;
            tnode = armath_new_node(
                "armath-eq-common armath-eq-common armath-eq-inline armath-eq-margins");
            
            while(pos<str.length){
                te=armath_nodify_arg("armath-eq-common armath-eq-vertical-element armath-eq-vertical-subelement-gap", str.substring(pos));
                if (!te) break;
                tnode.appendChild(te[0]);
                pos+=te[1];
            }
            parent.appendChild(tnode);
            pos-=1;
        }
        
        else if (str[pos] == "#") {
            var tr, td;
            tnode = document.createElement('table');
            tnode.setAttribute("class", "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-matrix");;
            
            while (pos<str.length && str[pos] == "#") {
                pos+=1;
                tr=document.createElement('tr');
                
                while(pos<str.length){
                    td=document.createElement('td');
                    td.setAttribute("class", "armath-eq-matrix-element armath-eq-vertical-subelement-gap");
                    te=armath_nodify_arg("armath-eq-common armath-eq-vertical-element", str.substring(pos));
                    if (!te) break;
                    td.appendChild(te[0]);
                    tr.appendChild(td);
                    pos+=te[1];
                }
                tnode.appendChild(tr);
                while (str[pos]==" " || str[pos]=="\t" || str[pos]=="\n") pos+=1;
            }
            
            parent.appendChild(tnode);   
            
            pos-=1;
        }
        
        /* (0)  if the current character matchs the overridable 
                directive symbol defined in the global 
                configuration section above */
                
        else if (str[pos] == "$"){
            tlen = arabic_follow_abjd(str.substring(pos+1));
            
            // return the same char if $ is a loner
            if (!tlen) parent.appendChild(
                armath_new_node(armath_compile.comm, "$"));
            
            var drctv = str.substring(pos+1, pos+tlen+1);
            if (!(drctv in lookup)) {
                console.warn("'" + drctv + "' is not a valid armath directive.");
                continue;
            }
            pos+=(tlen+1);
            // checkpoint A: checked
            if (drctv in lookup_mod) tstr=lookup_mod[drctv];
            else tstr = undefined;
            
            pos+= armath_fill_tree(parent, lookup[drctv], str.substring(pos), tstr);
            pos-=1; // to account for the auto invrement in this for loop
        }



        /* (1)  non abjdic directives MUST HAVE A lookup_mod ENTRY
                blank is okay */
        else if (str[pos] in lookup_mod){
            pos+= armath_fill_tree(
                        parent, 
                        lookup[str[pos]], 
                        str.substring(pos+1), 
                        lookup_mod[str[pos]]
                    ); 
        }
        
        
        /* (2)  surround the following paretheses block with 
                css parenthese */
        
        else if (str[pos]=="("){
            pos+= armath_fill_tree(parent, lookup["parentheses"], str.substring(pos));
            pos-=1;// for autoinvrement
          
        }
        else if (str[pos]=="|"){
            tlen=armath_get_parentheses(str.substring(pos), ["|", "|"]);
            if (tlen == -1){
                console.warn("no closing mark for an open '|'");
                continue;
            }
            armath_fill_tree(parent, lookup["absolute"], 
                "("+str.substring(pos+1, pos+tlen-1)+")");
            pos+=tlen-1;// for autoinvrement
          
        }
        else if (str[pos]=="["){
            tlen=armath_get_parentheses(str.substring(pos), ["[", "]"]);
            if (tlen == -1){
                console.warn("no closing mark for an open '['");
                continue;
            }
            armath_fill_tree(parent, lookup["square-brackets"], 
                "("+str.substring(pos+1, pos+tlen-1)+")");
            pos+=tlen-1;// for autoinvrement
          
        }
        
        /* (3)  consecutive numbers or letters  */
        else if (tpos=arabic_follow_abjd(str.substring(pos))){
            tstr=str.substring(pos, pos+tpos);
            parent.appendChild( armath_new_node( armath_compile.comm, tstr));
            pos+=tpos-1;
        }
        else if (tpos=arabic_follow_numral(str.substring(pos))){
            tstr=str.substring(pos, pos+tpos);
            parent.appendChild( armath_new_node( armath_compile.comm, tstr));
            pos+=tpos-1;
        }
        
        /* (-)  if none of the above filters have been triggered, 
                handle as an odd symbol */
        else {
            if ("\t\n ".indexOf(str[pos])>=0) continue;
            else if (str[pos] in lookup){
                // note: although pos has the potential of being updated here,
                // it realy, realy shouldn't.
                armath_fill_tree(parent, lookup[str[pos]], str.substring(pos));
            }
            else if (str[pos] == "@"){
                tstr = str[pos+1];
                parent.appendChild( armath_new_node( armath_compile.comm, tstr));
                pos+=1;
            }
            else{
                tstr = str[pos];
                parent.appendChild( armath_new_node( armath_compile.comm, tstr));
            }
        }
        
    }
    return pos;
}


function armath_init(){
    if (armath_is_init) return;
    var style=document.createElement("style");
    style.setAttribute("type", "text/css");
    //style.setAttribute("media", "screen, print");
    style.textContent=css_layout_def;
    document.getElementsByTagName("head")[0].appendChild(style);
    armath_is_init=true;
}

function armath_render_string(s)
{   
    armath_init();
    var n=armath_new_node("armath-eq");
    armath_compile(n, s);
    return n.outerHTML;
}
function armath_render(){
    armath_init();
    var lista=document.getElementsByClassName("armath-eq");
    var temp;
    for (i in lista) {
        temp=lista[i].innerHTML;
        lista[i].innerHTML="";
        armath_compile(lista[i], temp);
    }
                
}





