/*
    armathjs: lookup.js: contains the main translation table
    
    2012 Sulaiman A. Mustafa <http://people.sigh.asia/~sulaiman>    
    armathjs is public domain software.
    
    
    
    FORMAT
    
        lookup is used to translate armath directives to html. it has the 
        following structure*:
        
            lookup[d] = [ A ]
        
        where [A] is an array of As, and A is defined as:
        
            A = [ B, C ]
        
        each A is translated to a separate div. B is a string of classes 
        and must be provided ("" are okay). C can be:
        
            1.  `undefined` to sigify that armath should read from the 
                formating string.
            
            2.  a string which is inserted in the div
            
            3.  [ A ] for sub elements
        
        *lookup[d] can also be a string, and, if so, is simpally appended 
        to the parent element.


    LOOKUP_MOD
        
        lookup_mod is a companion array that modifies the behaviour of 
        armath. it holds a string of space seperated options which can
        be:
        
            1.  `reverse-pack` which indicates that the `undefined` 
                value should not be read from the formatting string
                but be the last div in the parent.
            
            2.  `reverse-embed` which indecates that the directive 
                should be rendered inside the last div in the parent.
               
            3. `remove-parentheses` removes the parantheses of the 
                last div. (only works with `reverse-pack`)
                
                
    CHARACTER_LOOKUP
    
        An key:value list characters that should be repaced should they 
        occure sepratly inside a div.
        
        
*/

var num_lookup=[ '٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩' ];

/**/
var lookup = new Array();
var lookup_mod = new Array(); // reverse pickup


lookup["إلى"]=
                '←';    
lookup["من"]=
                '→'; 
lookup["لانهاية"]=
                '∞'; 



// A: ثوابت
lookup["باي"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "π"]];
lookup["ثيتا"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "θ"]];
lookup["فاي"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "∅"]];
lookup["لامدا"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "λ"]];
lookup["دل"]=[["armath-eq-inline armath-eq-common armath-eq-margins armath-eq-flip", "∂"]];
lookup["دلتا"]=[["armath-eq-inline armath-eq-common armath-eq-margins", "Δ"]];


// A: المجموع والمضروب
lookup["مجموع"]=[["armath-eq-inline armath-eq-common armath-eq-stool", "ﳎ"]];
lookup["مضروب"]=[["armath-eq-inline armath-eq-common", "∏"]];

// calculus
lookup["تكامل"]=[
    [
        "armath-eq-inline armath-eq-common",
        [
            ["armath-eq-flip", "∫"]
        ]
    ]
];


// A: معادلات
lookup[">"]=lookup["أكبر"]=[["armath-eq-inline armath-eq-common armath-eq-operator", ">"]];
lookup["<"]=lookup["أصغر"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "<"]];
lookup["="]=lookup["يساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "="]];
lookup["+"]=lookup["موجب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "+"]];
lookup["-"]=lookup["سالب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "−"]];
lookup["*"]=lookup["ضرب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "×"]];
lookup["مولب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "±"]];
lookup["ساجب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "∓"]];
lookup["لايساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≠"]];
lookup["أكبريساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≥"]];
lookup["أصغريساوي"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≤"]];
lookup["تقريب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "≈"]];
lookup["تعريف"]=[
    [
        "armath-eq-inline armath-eq-common armath-eq-operator", 
            [
                ["", "="],
                ["armath-eq-common armath-eq-inline armath-eq-position-top armath-eq-position-center armath-eq-sub-2", "تعر"]
            ]
    ]
]
// A: منطق
lookup["جمع"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "⊕"]];
lookup["ضرب"]=[["armath-eq-inline armath-eq-common armath-eq-operator", "⊗"]];


// A: منطق
lookup["لكل"]='∀';
lookup["إذا"]='∴';
lookup["بماأن"]='∵';
lookup["مكافئ"]='⇔';
lookup["ينتج"]='⇒';
lookup["يؤدي"]='⇒';




// A: رسم
lookup["فراغ"]=[["armath-eq-common armath-eq-inline armath-eq-margins", ""]];
lookup["فأفقي"]=[["armath-eq-common armath-eq-inline armath-eq-horizontal-gap", ""]];
lookup["فرأسي"]=[["armath-eq-common armath-eq-inline armath-eq-vertical-gap", ""]];

lookup["ضخم"]=[["armath-eq-common armath-eq-inline armath-eq-heavy", undefined]];
lookup["دالة"]=[["armath-eq-common armath-eq-inline armath-eq-function", undefined]];
lookup["كبير"]=[["armath-eq-common armath-eq-inline armath-eq-larger", undefined]];
lookup["صغير"]=[["armath-eq-common armath-eq-inline armath-eq-smaller", undefined]];





lookup["^"]=[
    [
        "armath-eq-common armath-eq-inline", 
            [
                ["", undefined],
                ["armath-eq-common armath-eq-inline armath-eq-position-top armath-eq-sub armath-eq-margins", undefined]
            ]
    ]
];
lookup_mod["^"]="reverse-pack";

lookup["فوق"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-position-top armath-eq-position-center armath-eq-sub-2", undefined
    ]
];
lookup_mod["فوق"]="reverse-embed";



lookup["_"]=[
    [
        "armath-eq-common armath-eq-inline", 
            [
                ["", undefined],
                ["armath-eq-common armath-eq-inline armath-eq-position-bottom armath-eq-sub armath-eq-margins", undefined]
            ]
    ]
];
lookup_mod["_"]="reverse-pack";

lookup["تحت"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-position-bottom armath-eq-position-center right armath-eq-sub-2", undefined
    ]
];
lookup_mod["تحت"]="reverse-embed";


         
lookup["parentheses"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-parentheses",
        undefined
    ]
];

lookup["absolute"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-absolute",
        undefined
    ]
];

lookup["square-brackets"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-square-brackets",
            [
                ["armath-eq-common armath-eq-inline", undefined],
                ["armath-eq-square-brackets-tl", ""],
                ["armath-eq-square-brackets-tr", ""],
                ["armath-eq-square-brackets-bl", ""],
                ["armath-eq-square-brackets-br", ""]
            ]
    ]
];

lookup["سقف"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-square-brackets",
            [
                ["armath-eq-common armath-eq-inline", undefined],
                ["armath-eq-square-brackets-tl", ""],
                ["armath-eq-square-brackets-tr", ""]
            ]
    ]
];

lookup["قاع"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-margins armath-eq-square-brackets",
            [
                ["armath-eq-common armath-eq-inline", undefined],
                ["armath-eq-square-brackets-bl", ""],
                ["armath-eq-square-brackets-br", ""]
            ]
    ]
];




lookup["ند"]= [
    [
        "armath-eq-common armath-eq-inline armath-eq-fraction armath-eq-bar", undefined
    ]
];        

lookup["\\"]=
lookup["كسر"]= [
    [
        "armath-eq-common armath-eq-inline armath-eq-fraction armath-eq-margins", 
        [
            ["armath-eq-common armath-eq-inline", undefined],
            ["armath-eq-hr", ""],
            ["armath-eq-common armath-eq-inline armath-eq-fraction-denominator", undefined]
        ]
    ]
];        
lookup_mod["\\"]="reverse-pack remove-parentheses";

lookup["/"]=
lookup["جذر"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-root-container armath-eq-margins",
            [
                [
                    "armath-eq-sub armath-eq-root-degree", undefined
                ],
                [
                    "armath-eq-common armath-eq-inline armath-eq-root-body",
                    [
                        ["armath-eq-common armath-eq-inline armath-eq-root-dec", ""],
                        ["", undefined],
                        ["armath-eq-common armath-eq-inline armath-eq-root-falling", ""]
                    ]
                ]
            ]
    ]
];
lookup_mod["/"]="";



lookup["{"]=
lookup["أفرع"]=[
    [
        "armath-eq-common armath-eq-inline armath-eq-curly-container",
            [
                [ "armath-eq-curly-top-left", ""],
                [ "armath-eq-curly-top-right", ""],
                [ "armath-eq-curly-bottom-left", ""],
                [ "armath-eq-curly-bottom-right", ""],
                [ "armath-eq-common armath-eq-inline", undefined ]
            ]
    ]
];
lookup_mod["{"]="";


/*  Character translation lookup  [TODO]
    ============================
    
    This is a direct subsitution lookup dictionary where the key 
    should be replaced with the value.

*/
var character_lookup= new Array();

character_lookup["ن"]="Ͽ";
